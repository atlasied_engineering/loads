#include <stm32yyxx_ll_adc.h>
#include <stdio.h>
#include <math.h>
#include <SoftwareSerial.h>
#include <ezOutput.h>
#include <Chrono.h>

// Firmware Version
#define MAJOR_VERSION 2                  // major version 
#define MINOR_VERSION 0                 // minor version

// Dev Debug
int DEBUG_PRINTFS = 0;                   // if set to 1 it prints to serial console debug messages else set to 0 to turn off messages

// Serial
SoftwareSerial debug(PC11, PC10);        // RX, TX

// Load Box
#define MICRO_TEMP_THRLD  55             // micro temperature in celsius to kick on fans
#define I_SENSE_ABOVE_NOISE_FLOOR 1      // current threshold for when fans kick in at low speed
#define I_SENSE_FULL_SPEED_THRLD  2.75   // current threshold for when fans kick in at full speed
#define I_SENSE_MAX_SINK_CURRENT  12.5   // max current sink threshold
ezOutput pinPD2(PD2);                    // spare output to toggle with serial command
ezOutput pinPC8(PC8);                    // spare output to toggle with serial command
const int pinPC9 = PC9;                  // spare input  to read with serial command
const int pinPC12 = PC12;                // spare input  to read with serial command
ezOutput healthLED(PB15);                // health LED on board
ezOutput powerLED(PB0);                  // power indicator
const int fanSwitchPin = PB8;            // fan switch pin
ezOutput fanSwitchLED(PC13);             // fan switch LED
const int fanMode = PB13;                // fan modes
const int fanPwrSupply = PB14;           // fan power supply
const int relaySwitch1Pin = PB1;         // relay switch 1 pin
const int relaySwitch2Pin = PB5;         // relay switch 2 pin
const int relaySwitch3Pin = PB6;         // relay switch 3 pin
const int relaySwitch4Pin = PB7;         // relay switch 4 pin
ezOutput relaySwitchLED1(PC0);           // relay switch LED 1
ezOutput relaySwitchLED2(PC1);           // relay switch LED 2
ezOutput relaySwitchLED3(PC2);           // relay switch LED 3
ezOutput relaySwitchLED4(PC3);           // relay switch LED 4
const int relay1 = PB9;                  // relay pair 1
const int relay2 = PB10;                 // relay pair 2
const int relay3 = PB11;                 // relay pair 3
const int relay4 = PB12;                 // relay pair 4
const int totalSensingLines = 8;         // total number of sensing lines
const int currentLines[totalSensingLines] = { PA0, PA1, PC4, PC5, PA4, PA5, PA6, PA7 }; // current sensing lines
double currentReadings[totalSensingLines] = {0.0}; // buffer holding current sense lines readings
const int numberOfRelaySwitches = 4;       // number of relay switches
volatile int currentFanMode = 0;           // default fan mode NORMAL = 0, FULL SPEED = 1, FANS OFF = 2
Chrono tempTimer(Chrono::SECONDS);         // temperature timer set to be in seconds
Chrono currentNoiseFloorTimer(Chrono::SECONDS);      // current sensing timer set to be in seconds
Chrono aboveNoiseFloorCurrentTimer(Chrono::SECONDS); // current sensing timer set to be in seconds
Chrono currentReadsTimer(Chrono::MILLIS);            // current sensing timer set to be in seconds
bool isCurrentTooHigh = false;                       // flag for current sense lines being above the for max current threshold
/* Internal Temperature Sensor
   STM32F103x data-sheet:
   5.3.21 Temperature sensor characteristics
   Table 67. TS characteristics, Page 115
*/
#define CALX_TEMP 25
#if defined(STM32F1xx)
#define V25       1430
#define AVG_SLOPE 4300
#define VREFINT   1200
#endif
#define LL_ADC_RESOLUTION LL_ADC_RESOLUTION_12B // Analog read resolution 
#define ADC_RANGE 4096

void setup()
{
  // Load box
  analogReadResolution(12); // set resolution to 12-bit
  tempTimer.stop();
  currentNoiseFloorTimer.stop();
  aboveNoiseFloorCurrentTimer.stop();
  currentReadsTimer.stop();
  debug.begin(19200);  // initialize serial port to send and receive at 19200 baud
  pinMode(fanSwitchPin, INPUT_PULLUP);
  pinMode(relaySwitch1Pin, INPUT_PULLUP);
  pinMode(relaySwitch2Pin, INPUT_PULLUP);
  pinMode(relaySwitch3Pin, INPUT_PULLUP);
  pinMode(relaySwitch4Pin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(fanSwitchPin), on_fan_button_press, RISING); 
  attachInterrupt(digitalPinToInterrupt(relaySwitch1Pin), on_relay_switch_1_press, RISING); 
  attachInterrupt(digitalPinToInterrupt(relaySwitch2Pin), on_relay_switch_2_press, RISING); 
  attachInterrupt(digitalPinToInterrupt(relaySwitch3Pin), on_relay_switch_3_press, RISING); 
  attachInterrupt(digitalPinToInterrupt(relaySwitch4Pin), on_relay_switch_4_press, RISING); 
  pinMode(relay1, OUTPUT);
  pinMode(relay2, OUTPUT);
  pinMode(relay3, OUTPUT);
  pinMode(relay4, OUTPUT);
  for (int i = 0; i < totalSensingLines; i++) {
    pinMode(currentLines[i], INPUT);
  }
  pinMode(pinPC9, INPUT);
  pinMode(pinPC12, INPUT);
  pinMode(fanMode, OUTPUT);
  pinMode(fanPwrSupply,OUTPUT);
  at_power_on();  // run only at start up
}

void loop()
{
  load_box_main();    // load box main entry point
  serial_port_main(); // serial point main entry point
}

//
// Load Box
//
void set_LED(String which, String state) {
  unsigned long blinkRate = 500 / 2; // blink at a rate of 500 ms
  ezOutput* thisLED;

  if (!isCurrentTooHigh) {
    // Which LED
    if (which == "powerLED") {
      thisLED = &powerLED;
    } else if (which == "fanSwitchLED") {
      thisLED = &fanSwitchLED;
    } else if (which == "relaySwitchLED1") {
      thisLED = &relaySwitchLED1;
    } else if (which == "relaySwitchLED2") {
      thisLED = &relaySwitchLED2;
    } else if (which == "relaySwitchLED3") {
      thisLED = &relaySwitchLED3;
    } else if (which == "relaySwitchLED4") {
      thisLED = &relaySwitchLED4;
    }
    // Set LED State
    if (state == "high") {
      thisLED->high();
    } else if (state == "low") {
      thisLED->low();
    } else if (state == "toggle") {
      thisLED->toggle();
    } else if (state == "blink") {
      thisLED->blink(blinkRate, blinkRate);
    }
  }
}

void at_power_on() {
  // blink health LED at a rate of 500 ms
  healthLED.blink(250, 250);
  // Turn on power LED
  powerLED.high();
  // Turn off LEDs
  relaySwitchLED1.low();
  relaySwitchLED2.low();
  relaySwitchLED3.low();
  relaySwitchLED4.low();
  // Disengage relays
  digitalWrite(relay1, LOW);
  digitalWrite(relay2, LOW);
  digitalWrite(relay3, LOW);
  digitalWrite(relay4, LOW);
  // Set fans to normal mode
  set_fan_mode(currentFanMode);
  // Write to serial console
  char msg[20];
  sprintf(msg, "Welcome Load Box v%d.%d \n", MAJOR_VERSION, MINOR_VERSION);
  debug.println(msg);
}

void parallel_relays(int relayPair = 0, bool isBtnPressed = false) {
  int relayState = 0;
  int relayPin = 0;
  switch (relayPair) {
    case 1:
      relayPin = relay1;
      if (isBtnPressed) {
        set_LED("relaySwitchLED1", "toggle");
      }
      break;
    case 2:
      relayPin = relay2;
      if (isBtnPressed) {
        set_LED("relaySwitchLED2", "toggle");
      }
      break;
    case 3:
      relayPin = relay3;
      if (isBtnPressed) {
        set_LED("relaySwitchLED3", "toggle");
      }
      break;
    case 4:
      relayPin = relay4;
      if (isBtnPressed) {
        set_LED("relaySwitchLED4", "toggle");
      }
      break;
    default:
      isBtnPressed = false;
      break;

  }
  if (isBtnPressed) {
    relayState = digitalRead(relayPin);
    if (relayState == HIGH) {
      digitalWrite(relayPin, LOW);
    } else {
      digitalWrite(relayPin, HIGH);
    }
  }
}

void read_current_sensing_lines(double *buf, int buffSize) {
  double samples[buffSize] = {0.0};
  double vout[buffSize] = {0.0};
  int totalSamples = 150;
  double thisPeak = 0.0;

  // Take Samples
  for (int i = 0; i < totalSamples; i++) {
    for (int k = 0; k < buffSize; k++) {
      thisPeak = analogRead(currentLines[k]);
      if (thisPeak > samples[k]) {
        samples[k] = thisPeak;
      }
    }
  }

  // Calculate Reading
  for (int i = 0; i < buffSize; i++) {
    vout[i] = samples[i] * (3.285 / 4096.0);
      //if (i==0| i==1| i==4| i==5){                            //offset CH1, CH2, CH5, CH6 current calculation
        buf[i] = (abs((vout[i] - 1.65) / 0.044)) / sqrt(2) - 0.05;
      //}
      //else {
          //buf[i] = (abs((vout[i] - 1.65) / 0.044)) / sqrt(2); // (dcoffset - vout)/44mV/A taking from datasheet gives you peak then divide by sqrt root 2 to get RMS
      //}  
  }
}

void do_read_current_lines(unsigned long ms = 50) {
  if (!currentReadsTimer.isRunning()) {
    currentReadsTimer.restart();
  }
  if (currentReadsTimer.hasPassed(ms)) {
    read_current_sensing_lines(currentReadings, totalSensingLines);
    currentReadsTimer.restart();
  }
}

bool is_sinking_too_much_current() {
  for (int i = 0; i < totalSensingLines; i++) {
    if (currentReadings[i] > I_SENSE_MAX_SINK_CURRENT) {
      return true;
    }
  }
  return false;
}

void check_max_current_limit() 
{
  unsigned long blinkRate = 500 / 2; // blink at a rate of 500 ms
  isCurrentTooHigh = is_sinking_too_much_current();
  if (isCurrentTooHigh) {
    powerLED.blink(blinkRate, blinkRate);
    fanSwitchLED.blink(blinkRate, blinkRate);
    relaySwitchLED1.blink(blinkRate, blinkRate);
    relaySwitchLED2.blink(blinkRate, blinkRate);
    relaySwitchLED3.blink(blinkRate, blinkRate);
    relaySwitchLED4.blink(blinkRate, blinkRate);
  } else {
    // Power LED
    set_LED("powerLED", "high");
    // Fan Switch LED
    switch (currentFanMode)
    {
    case 0:
      // Normal Mode
      set_LED("fanSwitchLED", "high");
      break;
    case 1:
      // Full Speed
      set_LED("fanSwitchLED", "blink");
      break;
    case 2:
      // Fans Off
      set_LED("fanSwitchLED", "low");
      break;
    }
    // Switch LED 1
    int relayState = digitalRead(relay1);
    relayState == HIGH ? set_LED("relaySwitchLED1", "high") : set_LED("relaySwitchLED1", "low");
    // Switch LED 2
    relayState = digitalRead(relay2);
    relayState == HIGH ? set_LED("relaySwitchLED2", "high") : set_LED("relaySwitchLED2", "low");
    // Switch LED 3
    relayState = digitalRead(relay3);
    relayState == HIGH ? set_LED("relaySwitchLED3", "high") : set_LED("relaySwitchLED3", "low");
    // Switch LED 4
    relayState = digitalRead(relay4);
    relayState == HIGH ? set_LED("relaySwitchLED4", "high") : set_LED("relaySwitchLED4", "low");
  }
}

bool is_current_between_thresholds() {
  unsigned long noiseFloorTimeSec = 15 * 60;  // 15 mins
  unsigned long maxCurrentTimeSec = 10 * 60;  // 10 mins
  bool isCurrentHigh = false;
  double val;

  for (int i = 0; i < totalSensingLines; i++) {
    val = currentReadings[i];
    if (val > I_SENSE_FULL_SPEED_THRLD) {
      isCurrentHigh = true;
      currentNoiseFloorTimer.stop();
      // start timer and set fan mode high
      if (!aboveNoiseFloorCurrentTimer.isRunning()) {
        aboveNoiseFloorCurrentTimer.restart();
        digitalWrite(fanMode, HIGH);
        digitalWrite(fanPwrSupply, LOW);
      } else if (aboveNoiseFloorCurrentTimer.hasPassed(maxCurrentTimeSec)) {
        aboveNoiseFloorCurrentTimer.restart();
      }
    } else if (val > I_SENSE_ABOVE_NOISE_FLOOR && !aboveNoiseFloorCurrentTimer.isRunning()) {
      isCurrentHigh = true;
      // start timer and set fans supply low
      if (!currentNoiseFloorTimer.isRunning()) {
        currentNoiseFloorTimer.restart();
        digitalWrite(fanMode, LOW);
        digitalWrite(fanPwrSupply, LOW);
      } else if (currentNoiseFloorTimer.hasPassed(noiseFloorTimeSec)) {
        currentNoiseFloorTimer.restart();
      }
    }
  }

  if (currentNoiseFloorTimer.isRunning() && currentNoiseFloorTimer.hasPassed(noiseFloorTimeSec)) {
    currentNoiseFloorTimer.stop();
  } else if (aboveNoiseFloorCurrentTimer.isRunning() && aboveNoiseFloorCurrentTimer.hasPassed(maxCurrentTimeSec)) {
    aboveNoiseFloorCurrentTimer.stop();
  }
  return isCurrentHigh;
}

void set_fan_mode(int mode) {
  switch (mode) {
    case 0:
      // Normal Mode
      set_LED("fanSwitchLED", "high");
      if (!is_current_between_thresholds() && !currentNoiseFloorTimer.isRunning() && !aboveNoiseFloorCurrentTimer.isRunning()) {
        digitalWrite(fanMode, LOW);         // 9V option
        digitalWrite(fanPwrSupply, HIGH);    // kill switch ON. Device not being used.
      }
      break;
    case 1:
      // Full Speed
      digitalWrite(fanMode, HIGH);          // 12V option
      digitalWrite(fanPwrSupply, LOW);      // kill switch OFF
      set_LED("fanSwitchLED", "blink");
      break;
    case 2:
      // Fans Off
      digitalWrite(fanMode, LOW);         // 9V option
      digitalWrite(fanPwrSupply, HIGH);   // kill switch ON
      set_LED("fanSwitchLED", "low");
      break;
     case 5:
         // 9V Force ON
       set_LED("fanSwitchLED", "high");
       digitalWrite(fanMode, LOW);         // 9V option
       digitalWrite(fanPwrSupply, LOW);    // kill switch OFF
       
       break;
  }
}

void on_fan_mode_change() {
  // Stop timer if fan mode is not equal to normal mode
  if (currentFanMode != 0) {
    currentNoiseFloorTimer.stop();
    aboveNoiseFloorCurrentTimer.stop();
  }
  // Only change fan mode if micro temp timer is not running
  if (!tempTimer.isRunning()) {
    set_fan_mode(currentFanMode);
  }
}

static int32_t read_vref()
{
#ifdef __LL_ADC_CALC_VREFANALOG_VOLTAGE
  return (__LL_ADC_CALC_VREFANALOG_VOLTAGE(analogRead(AVREF), LL_ADC_RESOLUTION));
#else
  return (VREFINT * ADC_RANGE / analogRead(AVREF)); // ADC sample to mV
#endif
}

static int32_t read_temp_sensor(int32_t VRef)
{
#ifdef __LL_ADC_CALC_TEMPERATURE
  return (__LL_ADC_CALC_TEMPERATURE(VRef, analogRead(ATEMP), LL_ADC_RESOLUTION));
#elif defined(__LL_ADC_CALC_TEMPERATURE_TYP_PARAMS)
  return (__LL_ADC_CALC_TEMPERATURE_TYP_PARAMS(AVG_SLOPE, V25, CALX_TEMP, VRef, analogRead(ATEMP), LL_ADC_RESOLUTION));
#else
  return 0;
#endif
}

int32_t get_micro_temp() {
  int32_t vRef = read_vref();
  int32_t temp = read_temp_sensor(vRef);
  return temp;
}

void check_micro_temp() {
  unsigned long seconds = 15 * 60;  // 15 mins
  int32_t temp = get_micro_temp();  // get temperature

  if (temp > MICRO_TEMP_THRLD) {
    set_LED("powerLED", "blink");
    if (!tempTimer.isRunning()) {
      tempTimer.restart();  // set 15 min timer for fans to be ON
      // set fans to full speed
      digitalWrite(fanMode, HIGH);
      digitalWrite(fanPwrSupply, LOW);
    } else if (tempTimer.hasPassed(seconds)) {
      tempTimer.restart();  // Set 15 min timer for fans to be ON
    }
  } else {
    set_LED("powerLED", "high"); // reset power LED
    if (tempTimer.isRunning() && tempTimer.hasPassed(seconds)) {
      tempTimer.stop();
      set_fan_mode(currentFanMode); // reset fans to the previous mode
    }
  }
}

void on_fan_button_press() {
  currentFanMode++; // add one to transition to the next mode
  if (currentFanMode >= 3) {
    currentFanMode = 0;
  }
}

void on_relay_switch_1_press() {
  parallel_relays(1, true);
}

void on_relay_switch_2_press() {
  parallel_relays(2, true);
}

void on_relay_switch_3_press() {
  parallel_relays(3, true);
}

void on_relay_switch_4_press() {
  parallel_relays(4, true);
}

void load_box_main() {
  // MUST call the loop() function first
  powerLED.loop();
  healthLED.loop();
  fanSwitchLED.loop();
  relaySwitchLED1.loop();
  relaySwitchLED2.loop();
  relaySwitchLED3.loop();
  relaySwitchLED4.loop();
  // Function  Calls
  on_fan_mode_change();  
  do_read_current_lines(50); // read current every 50 ms
  check_max_current_limit();
  check_micro_temp();
}

//
// Serial
//
void debug_printfs(String msg) {
  if (DEBUG_PRINTFS > 0) {
    debug.println(msg);
  }
}

void print_input_options() {
  debug.println("Enter the corresponding letter of the command you wish to issue:");
  debug.println("\t a => Continuously display all current sense lines.");
  debug.println("\t b => Display all current sense lines once.");
  debug.println("\t c => Parallel Relay Pair 1.");
  debug.println("\t d => Parallel Relay Pair 2.");
  debug.println("\t e => Parallel Relay Pair 3.");
  debug.println("\t f => Parallel Relay Pair 4.");
  debug.println("\t g => Set fan mode to normal.");
  debug.println("\t h => Set fan mode to full speed.");
  debug.println("\t i => Set fans off.");
  debug.println("\t j => Toggle output pin PD2.");
  debug.println("\t k => Toggle output pin PC8.");
  debug.println("\t l => Continuously display input pin PC9 value.");
  debug.println("\t m => Continuously display input pin PC12 value.");
  debug.println("\t n => Display input pin PC9 value once.");
  debug.println("\t o => Display input pin PC12 value once.");
  debug.println("\t p => Continuously Display micro temperature.");
  debug.println("\t q => Display micro temperature once.");
  debug.println("\t r => Display firmware version.");
  debug.println("\t s => Enable developer debug messages.");
  debug.println("\t t => Stop all prints to console.");
  debug.println("\t u => Set fans ON to 9V supply.");
}

void serial_port_main() {
  static bool printCurrentLinesContinuously;
  static bool printFaultLinesContinuously;
  static bool printPinPC9ValContinuously;
  static bool printPinPC12ValContinuously;
  static bool printTempContinuously;

  if (debug.available()) {
    char incomingCharacter = debug.read();
    if (incomingCharacter != '\n') {
      switch (incomingCharacter) {
        case 'a':
          // Continuously display all current sense lines
          printCurrentLinesContinuously = true;
          break;
        case 'b':
          // Display all current sense lines once
          printCurrentLinesContinuously = false;
          for (int i = 0; i < totalSensingLines; i++) {
            debug.println("I_Sense " + String(i + 1) + " amperage value = " + String(currentReadings[i]));
              if (i == 7){
              debug.println("\n");
              }
          }
          break;
        case 'c':
          // Parallel Relay Pair 1
          parallel_relays(1, true);
          break;
        case 'd':
          // Parallel Relay Pair 2
          parallel_relays(2, true);
          break;
        case 'e':
          // Parallel Relay Pair 3
          parallel_relays(3, true);
          break;
        case 'f':
          // Parallel Relay Pair 4
          parallel_relays(4, true);
          break;
        case 'g':
          // Set fan mode to normal.
          currentFanMode = 0;
          set_fan_mode(currentFanMode);
          debug.println("Fans set to NORMAL MODE. \n");
          break;
        case 'h':
          // Set fan mode to full speed.
          currentFanMode = 1;
          set_fan_mode(currentFanMode);
          debug.println("Fans set to FULL SPEED. \n");
          break;
        case 'i':
          // Set fans off.
          currentFanMode = 2;
          set_fan_mode(currentFanMode);
          debug.println("Fans set to OFF. \n");
          break;
        case 'j':
          // Toggle output pin PD2
          pinPD2.toggle();
          break;
        case 'k':
          // Toggle output pin PC8
          pinPC8.toggle();
          break;
        case 'l':
          // Continuously display input pin PC9 value
          printPinPC9ValContinuously = true;
          break;
        case 'm':
          // Continuously display input pin PC12 value
          printPinPC12ValContinuously = true;
          break;
        case 'n':
          // Display input pin PC9 value once
          printPinPC9ValContinuously = false;
          debug.println("Input pin PC9 value = " + String(analogRead(pinPC9)) + "\n");
          break;
        case 'o':
          // Display input pin PC12 value once
          printPinPC12ValContinuously = false;
          debug.println("Input pin PC12 value = " + String(analogRead(pinPC12)) + "\n");
          break;
        case 'p':
          // Continuously display micro temperature
          printTempContinuously = true;
          break;
        case 'q':
          // Display micro temperature once
          printTempContinuously = false;
          debug.println("Micro Temp(°C) = " + String(get_micro_temp()) + "\n");
          break;
        case 'r':
          char msg[20];
          sprintf(msg, "Current Firmware v%d.%d \n", MAJOR_VERSION, MINOR_VERSION);
          debug.println(msg);
          break;
        case 's':
          DEBUG_PRINTFS = 1;
          break;
        case 't':
          // Stop all prints to console
          DEBUG_PRINTFS = 0;
          printCurrentLinesContinuously = false;
          printFaultLinesContinuously = false;
          printPinPC9ValContinuously = false;
          printPinPC12ValContinuously = false;
          printTempContinuously = false;
          break;
        case 'u':
          // Force fans to 9V supply
          currentFanMode = 5;
          set_fan_mode(currentFanMode);
          debug.println("Fans set to 9V. \n");
          break;
        default:
          print_input_options();
          break;
      }
    }
  }

  // Continuously display all current sense lines
  if (printCurrentLinesContinuously) {
    for (int i = 0; i < totalSensingLines; i++) {
      debug.println("I_Sense " + String(i + 1) + " amperage value = " + String(currentReadings[i]));
      if (i == 7){
        debug.println("\n");
      }
    }
  }
  // Continuously display input pin PC9 value once
  if (printPinPC9ValContinuously) {
    debug.println("Input pin PC9 value = " + String(analogRead(pinPC9)) + "\n");
  }
  // Continuously display input pin PC12 value
  if (printPinPC12ValContinuously) {
    debug.println("Input pin PC12 value = " + String(analogRead(pinPC12)) + "\n");
  }
  // Continuously display micro temperature
  if (printTempContinuously) {
    debug.println("Micro Temp(°C) = " + String(get_micro_temp()) + "\n");
  }
}
